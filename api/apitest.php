<?php


class apitest extends apiBaseClass
{

    function fileapione(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/my-img.jpg';
        file_put_contents($path, file_get_contents($_POST['linktoapi']));
        return 'ok';
    }
    function fileapipost(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        $dir = '../zip/';
        $maxLength = 6;
        if (is_dir($dir)) {
            $arrayFiles = array_diff(scandir($dir, 1), ['..', '.']);
            $fileName = '000001';

            if (!empty($arrayFiles[0])) {
                $lastFileName = (int)$arrayFiles[0];
                $fileName = $lastFileName + 1;
                $needZero = $maxLength - strlen((string)$fileName);

                for ($counter = 0; $counter < $needZero; $counter++) {
                    $fileName = '0' . $fileName;
                }
            }
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $file_path = 'zip/' . $fileName . '.' . $ext;
            if (0 >= $_FILES['file']['error']) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/' . $file_path)) {
                    return [
                        'path' => $_SERVER['SERVER_NAME'] . '/' . $file_path,
                        'file_name' => $fileName
                    ];
                }
            }
        }
        return false;
    }
    //http://www.example.com/api/?apitest.helloAPI={}
    function helloAPI()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        $retJSON = $this->createDefaultJson();
        $retJSON->withoutParams = 'It\'s method called without parameters';
        return $retJSON;
    }

    //http://www.example.com/api/?apitest.helloAPIWithParams={"TestParamOne":"Text of first parameter"}
    function helloAPIWithParams($apiMethodParams)
    {
        $retJSON = $this->createDefaultJson();
        if (isset($apiMethodParams->TestParamOne)) {
            //Все ок параметры верные, их и вернем
            $retJSON->retParameter = $apiMethodParams->TestParamOne;
        } else {
            $retJSON->errorno = APIConstants::$ERROR_PARAMS;
        }
        return $retJSON;
    }

    //http://www.example.com/api/?apitest.helloAPIResponseBinary={"responseBinary":1}
    function helloAPIResponseBinary($apiMethodParams)
    {
        header('Content-type: image/png');
        echo file_get_contents("http://habrahabr.ru/i/error-404-monster.jpg");
    }

}

