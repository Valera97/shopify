<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
$apiKey = "83717fa6ea25534b419e534b991bc4f2";
$pwd = "4ba160100ad5da58277719d186eae419";

$baseUrl = "https://" . $apiKey . ":" . $pwd . "@pingbigstore.myshopify.com";

//Size
const SMALL = 1;
const MEDIUM = 2;
const LARGE = 3;
const XL = 4;
const XXL = 5;

//Type

const HOODIE = '01';
const SWEATSHIRT = '02';
const TSHIRT_MALE = '03';
const TSHIRT_FEMALE = '04';
const ZIPPER_HOODIE = '07';

$sku = '';

if ($_POST['name'] === 'T-Shirt-Male') {
    $sku = $sku . TSHIRT_MALE;
} elseif ($_POST['name'] === 'Sweatshirts') {
    $sku = $sku . SWEATSHIRT;
} elseif ($_POST['name'] === 'Zipper Hoodie') {
    $sku = $sku . ZIPPER_HOODIE;
} elseif ($_POST['name'] === 'Hoodie') {
    $sku = $sku . HOODIE;
} elseif ($_POST['name'] === 'T-Shirt-Female') {
    $sku = $sku . TSHIRT_FEMALE;
} else {
    $sku = $sku . '08';
}

if ($_POST['size'] === 'S') {
    $sku = $sku . SMALL;
} elseif ($_POST['size'] === 'M') {
    $sku = $sku . MEDIUM;
} elseif ($_POST['size'] === 'L') {
    $sku = $sku . LARGE;
} elseif ($_POST['size'] === 'XL') {
    $sku = $sku . XL;
} elseif ($_POST['size'] === 'XXL') {
    $sku = $sku . XXL;
} else {
    $sku = $sku . 6;
}
$sku = $sku . $_POST['content']['file_name'];

$date_time = new DateTime();
$current_time = $date_time->format('d_m_Y_H_i_s_u');
$file_title = "file_{$current_time}.jpg";
$file_path = 'uploads/' . $file_title;
$ifp = fopen( $file_path, 'wb' );

// split the string on commas
// $data[ 0 ] == "data:image/png;base64"
// $data[ 1 ] == <actual base64 string>
$data = explode( ',', 'data:image/jpg;base64,'.$_POST['screenshot']);
// we could add validation here with ensuring count( $data ) > 1
fwrite( $ifp, base64_decode( $data[ 1 ] ) );

// clean up the file resource
fclose( $ifp );

$product =
    array('title' => $_POST['name'].' '.$_POST['content']['file_name'],
        'body_html' => 'Size: '.$_POST['size'],
        'vendor'=> 'My Product Vendor',
        'product_type'=> '3D customizer',
        'variants' => array(
            array('option1' => 'Default',
                'price' => $_POST['price'],
                'inventory_quantity'=> '1',
                'inventory_management' => 'shopify',
                'taxable' => true,
                'requires_shipping' => true,
                'sku' => $sku,
                'barcode' => $_POST['content']['file_name'],
            )
        ),
    );
$ch = curl_init($baseUrl.'/admin/products.json'); //set the url
$data_string = json_encode(array('product'=>$product)); //encode the product as json
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  //specify this as a POST
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); //set the POST string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //specify return value as string
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
); //specify that this is a JSON call
$server_output_true = curl_exec ($ch); //get server output if you wish to error handle / debug
//get server output if you wish to error handle / debug

$test= json_decode($server_output_true,true);
$id= $test['product']['id'];
curl_close ($ch); //close the connection
$image = array(
    'image' => [
        'attachment'=> $_POST['screenshot'],
        'filename' => 'image_post.jpg'
    ]
);
$curl = curl_init();
$base64_img = $_POST['screenshot'];
$url ="https://83717fa6ea25534b419e534b991bc4f2:4ba160100ad5da58277719d186eae419@pingbigstore.myshopify.com/admin/api/2019-10/products/".$id."/images.json";
$jsonDataEncoded = json_encode($image);
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
echo $server_output_true;
exit();